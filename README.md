
# pgtooora Proof of Concept

The purpose of this laboratory is to replicate events and sychronize data from a Postgres to an Oracle instance, using Debezium backed up with Kafka layer.


- Choose Debezium version


```sh
export DEBEZIUM_VERSION=2.1
```

- Docker compose up:


```shell
# Start the topology as defined in https://debezium.io/documentation/reference/stable/tutorial.html
docker compose -f docker-compose.yml up
```

> Note: V1 compose is deprecated, you need to [install compose plugin](https://docs.docker.com/compose/install/linux/#install-using-the-repository).

- Verify Kafka API availavility:

```shell
curl  -X GET  -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:18083/ | jq .
```

```json
{
  "version": "3.3.1",
  "commit": "e23c59d00e687ff5",
  "kafka_cluster_id": "3h7jvAa2QNKqGoA9VcnRAA"
}
```

## Using Postgres


```shell
# Start Postgres connector
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:18083/connectors/ -d @register-postgres.json

```
_Note: Make sure set `plugin.name` to `pgoutput` to avoid the default value plugin `decoderbufs`_

output:
```
HTTP/1.1 201 Created
Date: Sat, 18 Mar 2023 23:10:22 GMT
Location: http://localhost:18083/connectors/inventory-connector
Content-Type: application/json
Content-Length: 408
Server: Jetty(9.4.48.v20220622)
```

- Verify connector status:


```
curl  -X GET     http://localhost:18083/connectors/inventory-connector/status | jq .
```

output:
```
{
  "name": "inventory-connector",
  "connector": {
    "state": "RUNNING",
    "worker_id": "172.20.0.6:8083"
  },
  "tasks": [
    {
      "id": 0,
      "state": "RUNNING",
      "worker_id": "172.20.0.6:8083"
    }
  ],
  "type": "source"
}
```

Also you can check the replication slot created in the database and the publication:

```

docker compose exec postgres /bin/bash
root@8c9dba45d930:/# psql
psql: error: connection to server on socket "/var/run/postgresql/.s.PGSQL.5432" failed: FATAL:  role "root" does not exist
root@8c9dba45d930:/# su - postgres
postgres@8c9dba45d930:~$ psql -d inventory
psql (14.7 (Debian 14.7-1.pgdg110+1))
Type "help" for help.

invetory=# 

inventory=# select * from pg_replication_slots ;
 slot_name |  plugin  | slot_type | datoid | database  | temporary | active | active_pid | xmin | catalog_xmin | restart_lsn | confirmed_flush_lsn | wal_status | safe_wal_size | two_p
hase 
-----------+----------+-----------+--------+-----------+-----------+--------+------------+------+--------------+-------------+---------------------+------------+---------------+------
-----
 debezium  | pgoutput | logical   |  16384 | inventory | f         | t      |        279 |      |          794 | 0/22F87F8   | 0/22F8830           | reserved   |               | f
(1 row)

inventory=# \dRp
                                     List of publications
      Name       |  Owner   | All tables | Inserts | Updates | Deletes | Truncates | Via root 
-----------------+----------+------------+---------+---------+---------+-----------+----------
 dbz_publication | postgres | t          | t       | t       | t       | t         | f
(1 row)

inventory=# \dn
  List of schemas
  Name  |  Owner   
--------+----------
 public | postgres
(1 row)

inventory=# \d+
                                      List of relations
 Schema |  Name  | Type  |  Owner   | Persistence | Access method |    Size    | Description 
--------+--------+-------+----------+-------------+---------------+------------+-------------
 public | prueba | table | postgres | permanent   | heap          | 8192 bytes | 
(1 row)

inventory=# \dRp+
                        Publication dbz_publication
  Owner   | All tables | Inserts | Updates | Deletes | Truncates | Via root 
----------+------------+---------+---------+---------+-----------+----------
 postgres | t          | t       | t       | t       | t         | f
(1 row)

inventory=# select * from pg_publication_tables;
     pubname     | schemaname | tablename 
-----------------+------------+-----------
 dbz_publication | public     | prueba
(1 row)

```

# Consume messages from a Debezium topic
```
docker compose exec kafka /kafka/bin/kafka-console-consumer.sh \
    --bootstrap-server kafka:9092 \
    --from-beginning \
    --property print.key=true \
    --topic pgserver
```

# Modify records in the database via Postgres client
docker-compose -f docker-compose.yml exec postgres env PGOPTIONS="--search_path=inventory" bash -c 'psql -U $POSTGRES_USER postgres'

# Shut down the cluster
docker-compose -f docker-compose.yml down
```

Example output:

```bash
-> % curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:18083/connectors/ -d @register-postgres.json
HTTP/1.1 201 Created
Date: Wed, 01 Mar 2023 14:24:20 GMT
Location: http://localhost:18083/connectors/inventory-connector
Content-Type: application/json
Content-Length: 386
Server: Jetty(9.4.48.v20220622)
```

```json
{"name":"inventory-connector","config":{"connector.class":"io.debezium.connector.postgresql.PostgresConnector","tasks.max":"1","database.hostname":"postgres","database.port":"5432","database.user":"postgres","database.password":"postgres","database.dbname":"postgres","topic.prefix":"dbserver1","schema.include.list":"inventory","name":"inventory-connector"},"tasks":[],"type":"source"}%      
```


## Using Oracle

This assumes Oracle is running on localhost
(or is reachable there, e.g. by means of running it within a VM or Docker container with appropriate port configurations)
and set up with the configuration, users and grants described in the Debezium [Vagrant set-up](https://github.com/debezium/oracle-vagrant-box).

```shell
# Start the topology as defined in https://debezium.io/documentation/reference/stable/tutorial.html
export DEBEZIUM_VERSION=2.1
docker compose -f docker-compose.yml up build

# Insert test data
cat debezium-with-oracle-jdbc/init/inventory.sql | docker exec -i dbz_oracle sqlplus debezium/dbz@//localhost:1521/ORCLPDB1
```

The Oracle connector can be used to interact with Oracle either using the Oracle LogMiner API or the Oracle XStreams API.

### LogMiner

The connector by default will use Oracle LogMiner.
Adjust the host name of the database server in the `register-oracle-logminer.json` as per your environment.
Then register the Debezium Oracle connector:

```shell
# Start Oracle connector
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:18084/connectors/ -d @register-oracle-logminer.json

# Create a test change record
echo "INSERT INTO customers VALUES (NULL, 'John', 'Doe', 'john.doe@example.com');" | docker exec -i dbz_oracle sqlplus debezium/dbz@//localhost:1521/ORCLPDB1

# Consume messages from a Debezium topic
docker-compose -f docker-compose-oracle.yaml exec kafka /kafka/bin/kafka-console-consumer.sh \
    --bootstrap-server kafka:9092 \
    --from-beginning \
    --property print.key=true \
    --topic server1.DEBEZIUM.CUSTOMERS

# Modify other records in the database via Oracle client
docker exec -i dbz_oracle sqlplus debezium/dbz@//localhost:1521/ORCLPDB1

# Shut down the cluster
docker-compose -f docker-compose-oracle.yaml down
```

### Example output

The below example shows that both connectors use the same DCS, so inventories
are visible through connectors:

```bash
emanuel@tails [03:24:21 PM] [~/Labs/pgtoora_poc] [main *]
-> % curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:18084/connectors/ -d @register-oracle-logminer.json

HTTP/1.1 409 Conflict
Date: Wed, 01 Mar 2023 14:25:52 GMT
Content-Type: application/json
Content-Length: 75
Server: Jetty(9.4.48.v20220622)

{"error_code":409,"message":"Connector inventory-connector already exists"}%                                                                                                
emanuel@tails [03:25:52 PM] [~/Labs/pgtoora_poc] [main *]
-> % curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:18084/connectors/ -d @register-oracle-logminer.json

HTTP/1.1 201 Created
Date: Wed, 01 Mar 2023 14:26:48 GMT
Location: http://localhost:18084/connectors/inventory-connector-oracle
Content-Type: application/json
Content-Length: 508
Server: Jetty(9.4.48.v20220622)

{"name":"inventory-connector-oracle","config":{"connector.class":"io.debezium.connector.oracle.OracleConnector","tasks.max":"1","topic.prefix":"server1","database.hostname":"oracle11g","database.port":"1521","database.user":"system","database.password":"oracle","database.dbname":"xe","database.pdb.name":"ORCLPDB1","schema.history.internal.kafka.bootstrap.servers":"kafka:9092","schema.history.internal.kafka.topic":"schema-changes.inventory","name":"inventory-connector-oracle"},"tasks":[],"type":"source"}%  
```
