# Documentation

- [Oracle Connector documentation](https://debezium.io/documentation/reference/stable/connectors/oracle.html#oracle-example-configuration).
- [Tutorial](https://debezium.io/documentation/reference/stable/tutorial.html)
- [RestAPI](https://docs.confluent.io/platform/current/connect/references/restapi.html)
- [PostgreSQL Connector documentation](https://debezium.io/documentation/reference/stable/connectors/postgresql.html)

# Connectors

[More details](https://stackoverflow.com/a/68105726)

Remove connector:


```bash
curl -i -X DELETE localhost:18083/connectors/inventory-connector/
```

Update configuration:


```bash
curl -i -X PUT -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:18083/connectors/inventory-connector/config -d <json>
```

 
