#!/bin/bash
set -e

docker-compose down && rm -rf data/  && \
    sudo rm -rf pgdata/ && sudo rm -rf oradata/
